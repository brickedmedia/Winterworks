require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Api
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version & add assets.
    config.load_defaults 5.1
    config.autoload_paths << Rails.root.join('lib') #require lib files
    config.assets.paths << Rails.root.join('vendor', 'assets', 'images', 'javascripts', 'fonts')

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
