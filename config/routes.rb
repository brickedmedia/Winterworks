Rails.application.routes.draw do


  mount RailsAdmin::Engine => '/dev-admin', as: 'rails_admin'
  ActiveAdmin.routes(self)
  /# Implement subdomain for blog #/

  # constraints Subdomain do ----> 'Blog'
  # get "/" => redirect { |params| "http://www.winterworks.com" }
  # get '*path' => redirect('/')
  # get '*path#*path' => redirect('/')
  # end

  # admin routes

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    passwords: 'users/passwords',
    registrations: 'users/registrations'
}, skip: [:sessions, :registrations]


  root :to => "index#default"
  get '/dashboard/' => 'users#index', :as => 'user_root' # manage projects from here? idk yet -- definitely view orders and stuff

  # Devise 

  devise_scope :user do

    ## MARK: Add custom forget password button

    get    'register', to: 'devise/registrations#new',    as: :new_user_registration
    post   '/account',  to: 'devise/registrations#create'
    get    '/account/cancel', to: 'devise/registrations#cancel', as: :cancel_user_registration

    get    '/account',  to: 'devise/registrations#edit',   as: :edit_user_registration
    put    '/account',  to: 'devise/registrations#update'
    patch  '/account',  to: 'devise/registrations#update', as: :user_registration
    delete '/account',  to: 'devise/registrations#destroy'
    
    get    'login',  to: 'devise/sessions#new',     as: :new_user_session
    post   'login',  to: 'devise/sessions#create',  as: :user_session
    delete 'logout', to: 'devise/sessions#destroy', as: :destroy_user_session
  end

  # Extraneous routes --> Static

  get '/pricing/' => 'index#pricing', as: 'pricing'
  get '/about/' => 'index#about', as: 'about'
  get '/blog/' => 'index#blog', as: 'view_blog'
  get '/support/' => 'index#support', as: 'support'

  # Extraneous routes --> Dynamic/Static

  get '/browse/' => 'index#discover', as: 'social_feed'
  get '/browse/posts/' => 'posts#list_global', as: 'posts_feed'

  get '/browse/users/' => 'users#list', as: 'list_users'
  get '/browse/talent/' => 'users#requests', as: 'hireable_users'

  # Extraneous routes --> Project Management

  get '/browse/projects/:q' => "projects#filter_by_project_type", :as => 'filter_proj_by_type'
  
  match '/edit-project/:uuid/:id' => 'edit_project#show', :via => [:get], as: 'edit_project'


  get '/browse/projects/' => 'projects#feed', as: 'view_projects'
  get '/new/project/' => 'projects#new', as: 'create_new_project'
  post '/submit/project/' => 'projects#create', as: 'submit_new_project'
  match '/project/:uuid/' => 'projects#view', :via => [:get], as: 'view_singular_project'

  # Extraneous routes --> Commission Management

  match '/@:username/commission/:uuid/' => 'commissions#view', :via => [:get], as: 'view_singular_commission'
  get '/browse/commissions/' => 'commissions#list', as: 'list_commissions'
  get '/new/commissions/' => 'commissions#create', as: 'create_new_commission'
  post '/submit/commission/' => 'commissions#new', as: 'submit_new_commission'

  # Extraneous routes --> Upload, Creation + Onboarding

  get '/submit/' => 'posts#upload', as: 'upload_post'
  post '/submit/' => 'posts#new', as: 'submit_new_post'
  match '/posts/:uuid/' => 'posts#view', :via => [:get], as: 'view_this_post'
  match '/posts/:uuid/like' => 'posts#like', :via => [:post], as: 'like_post'
  match '/posts/:uuid/comment' => 'comments#create', :via => [:post], as: 'create_new_comment'

  # Extraneous routes --> Public User Showcase
  match '/@:username/follow' => 'followerships#follow_user', :via => [:post], as: 'follow_this_user'


  match '/@:username/following' => 'followerships#following', :via => [:get], as: 'user_following'
  match '/@:username/followers' => 'followerships#followers', :via => [:get], as: 'user_followers'
  match '/@:username/likes' => 'users#list_likes', :via => [:get], as: 'list_this_users_likes'


  match '/@:username' => 'users#show', :via => [:get], as: 'view_profile'
  match '/@:username/about' => 'users#more_info', :via => [:get], as: 'about_profile'

  get '/profile/edit/' => 'users#edit_profile', as: 'edit_profile'
  patch '/profile/edit/' => 'users#update_profile', as: 'update_profile'

  get '/i/notifications/' => 'users#notifications', as: 'social_notifications'

  # Extraneous routes --> Commissions

  match '/@:username/portfolio/' => 'portfolio#show', :via => [:get], as: 'view_user_commission_page'
  match '/@:username/portfolio/edit/' => 'portfolio#edit', :via => [:get], as: 'edit_user_commission_page'
  match '/@:username/portfolio/edit/' => 'portfolio#update', :via => [:patch], as: 'update_user_commission_page'
  
  # Extraneous routes --> Manufactory Checkout
  
  resources :manufactory

  # Extraneous routes --> Orders

  match '/checkout/commission/:uuid/:id' => 'orders#new', :via => [:get], as: 'place_new_commission_order'

  # Extraneous routes --> Dynamic Project Space

  # Extraneous routes --> Payments

  get '/account/billing', to: 'users#payments', as: 'edit_billing_methods'

end
