class AddWorkExpToProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :work_exp, :text
  end
end
