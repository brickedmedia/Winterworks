class AddUuidToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :uuid, :string
  end
end
