class AddDescToCommission < ActiveRecord::Migration[5.2]
  def change
    add_column :commissions, :desc, :text
  end
end
