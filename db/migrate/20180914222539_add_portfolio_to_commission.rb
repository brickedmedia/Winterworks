class AddPortfolioToCommission < ActiveRecord::Migration[5.2]
  def change
    add_reference :commissions, :portfolio, index: true
    add_foreign_key :commissions, :portfolios
  end
end
