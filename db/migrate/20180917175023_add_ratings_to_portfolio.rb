class AddRatingsToPortfolio < ActiveRecord::Migration[5.2]
  def change
    add_column :portfolios, :ratings, :float
  end
end
