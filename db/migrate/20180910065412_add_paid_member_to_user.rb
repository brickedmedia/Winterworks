class AddPaidMemberToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :paid_member, :boolean, default: false
  end
end
