class CreateCommissions < ActiveRecord::Migration[5.2]
  def change
    create_table :commissions do |t|
      t.float :price
      t.references :user, foreign_key: true
      t.string :title

      t.timestamps
    end
  end
end
