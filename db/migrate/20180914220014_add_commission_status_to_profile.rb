class AddCommissionStatusToProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :taking_requests, :boolean, default: false
  end
end
