class AddTypeToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :type, :integer, default: 1
  end
end
