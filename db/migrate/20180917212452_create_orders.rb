class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :user, foreign_key: true
      t.references :commission, foreign_key: true
      t.decimal :amount

      t.timestamps
    end
  end
end
