class AddRateTypeToPortfolio < ActiveRecord::Migration[5.2]
  def change
    add_column :portfolios, :rate_type, :integer
  end
end
