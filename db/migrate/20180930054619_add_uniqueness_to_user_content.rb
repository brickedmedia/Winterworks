class AddUniquenessToUserContent < ActiveRecord::Migration[5.2]
  def change
    #index: {unique: true}
    add_reference :posts, :uuid, index: {unique: true} #post
    add_reference :projects, :uuid, index: {unique: true} #project
    add_reference :commissions, :uuid, index: {unique: true} #commission
    add_reference :orders, :uuid, index: {unique: true} #order
  end
end
