class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.text :about_desc
      t.integer :status, default: 1 # 1 means ur not looking for comissions
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
