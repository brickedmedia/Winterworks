class AddAccessColumnsToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :date_posted, :date
    add_column :projects, :is_published, :boolean, default: false
  end
end
