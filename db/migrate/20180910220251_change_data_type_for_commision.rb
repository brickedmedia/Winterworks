class ChangeDataTypeForCommision < ActiveRecord::Migration[5.2]
  def change
    change_column :commissions, :price, :decimal
  end
end
