class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :description
      t.date :date_posted
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
