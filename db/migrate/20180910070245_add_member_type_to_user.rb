class AddMemberTypeToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :member_type, :integer, default: 1
  end
end
