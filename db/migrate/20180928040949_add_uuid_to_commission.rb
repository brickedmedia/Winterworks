class AddUuidToCommission < ActiveRecord::Migration[5.2]
  def change
    add_column :commissions, :uuid, :string
  end
end
