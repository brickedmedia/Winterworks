// index.js

$(document).ready(function(){
    $('.nav-right-side img').on('click', () => {
        console.log("clicked");
        if ($('#navbar .wrap .nav-right-side .dropdown-menu').hasClass('active')) {
            // if it already has, remove it and make the menu invisible
            $('#navbar .wrap .nav-right-side .dropdown-menu').removeClass('active');
            $('#navbar .wrap .nav-right-side .dropdown-menu').css({'display': ''});
            $('#navbar .wrap .nav-right-side .dropdown-menu').css({'display': 'none'});

            next();
        } else {
            console.log("twice clicked");
            // if it doesn't, add the class and make the menu visible
            $('#navbar .wrap .nav-right-side .dropdown-menu').addClass('active');
            $('#navbar .wrap .nav-right-side .dropdown-menu').css({'display': ''});
            $('#navbar .wrap .nav-right-side .dropdown-menu').css({"display": "block"});

            next();
        };
    });
});

