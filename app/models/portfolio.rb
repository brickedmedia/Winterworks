# bin/rails g model Portfolio 
# user:references
# description:text

# rate_amount:decimal
# rate_timeframe:integer

# ratings:float (avg'd for testimonials)
# rate_type: item/hour
#
# rate_status = `rate_amount` per Y item/hour.
#

class Portfolio < ApplicationRecord
  enum rate_type: { item: 1, hours: 0} 

  belongs_to :user
  has_many :commissions, dependent: :destroy
  accepts_nested_attributes_for :commissions
end
