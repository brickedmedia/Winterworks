#
#
#
# related to portfolio. a user can have a `commission portfolio` and sell `commissions`
#
#
# title: string
# desc: text
# artist: user reference
# price: float
# example_image: attachment
#
#
# Commissiosn
# availibility_status = "accepting comissions" 1 | "not accepting comissions"
#

# rails g migration AddUUIDToCommission uuid:string

class Commission < ApplicationRecord

  belongs_to :user
  belongs_to :portfolio

  # Attachments
  has_one_attached :example_image

  # Validations

  validates(:uuid, presence: true, :uniqueness => {:message => "This commission already exists."})

  validates(:price, presence: true)

  validates(:example_image, presence: true)

  validates(:title, presence: true, length: 5..55)

end