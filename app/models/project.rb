# rails g model project title:string project_type:integer category:integer user:references
# rails g migration AddAccessColumnsToProject date_posted:date is_published:boolean
# rails g migration AddStatusSettingsToProject status:integer
# rails g migration AddUUIDToProject uuid:string

class Project < ApplicationRecord
  belongs_to :user
  # Titles for project types
  enum project_type: { Volunteer: 2, Hobby: 1, Funded: 0}
  # Titles for categories
  enum category: { Action:4, Horror: 3, Anime: 2, Fairytale: 1, Scifi: 0}
  # Titles for status settings
  enum status: {"Final Editing": 4, "Post Production": 3, Production:2, "Pre-Production": 1, Development: 0}

    # Validations

    validates(:date_posted, presence: true)    

    validates(:uuid, presence: true, :uniqueness => {:message => "This project already exists."})

    validates(:status, presence: true)

    validates(:project_type, presence: true)

    validates(:category, presence: true)
  
    validates(:title, presence: true, length: 5..55)
end
