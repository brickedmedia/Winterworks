# == Schema Information
#
# Table name: users
#
# id			:integer		not null, primary_key
# name			:string(255)
# username      :string(255)
#
# bio:text
# location: string
# website: string --- URL REGEX
#
# paid_member: boolean (if yes, access certain features depending on....
#
# member_type: integer ---- {0: Creator ($19.99), 1: Free, 2: Basic ($4.99), 3: Full ($9.99)}
#
#

class User < ApplicationRecord

  enum member_type: { full: 3, basic: 2, free: 1, creator: 0}

  # Attachments & Associations

  has_many :followers, :class_name => 'Following', :foreign_key => 'user_id'
  has_many :following, :class_name => 'Following', :foreign_key => 'follower_id'

  has_one_attached :profile_image

  has_many :likes, dependent: :destroy #post likes
  has_many :posts, dependent: :destroy
  has_many :liked_posts, :through => :likes, :source => :post #@user.liked_posts

  has_many :commissions, dependent: :destroy
  has_many :projects, dependent: :destroy

  # User Profile

  has_one :profile, dependent: :destroy
  after_create :build_profile
  accepts_nested_attributes_for :profile

  # Commission Portfolio
  has_one :portfolio, dependent: :destroy
  after_create :build_portfolio
  accepts_nested_attributes_for :portfolio

  # Regex Expressions

  VALID_URL_REGEX = /\A(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\z/ix

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  # :validatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable,
         :trackable

  # Validations

  validates(:email, presence: true, uniqueness: { case_sensitive: false })

  validates(:encrypted_password, presence: true, length: { minimum: 8 })

  validates(:member_type, presence: true)

  validates(:username, presence: true, length: 3..16, uniqueness: { case_sensitive: false })

  validates(:first_name, presence: true, length: { maximum: 50 })
  
  validates(:last_name, presence: true, length: { maximum: 50 })

  private
    def build_profile
      self.profile = Profile.create(:user => self)
    end 

    def build_portfolio
      self.portfolio = Portfolio.create(:user => self)
    end
end
