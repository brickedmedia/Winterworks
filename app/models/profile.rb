# user.profile
# about_desc: detailed description about the user
# status: what they're looking for right nwo
#  
# hero_image : attachment (add validatins for png/jpg file)
#
# implement this -> [ hold stats, following, commission portfolio, post likes, activity, status]
#
#
# taking_requests: whether user is taking commisions or not, default is false
#
# add skills to profile

class Profile < ApplicationRecord
  belongs_to :user
  enum status: { "In a project": 4, "Away": 3, "Looking to Collaborate": 2, "Looking for Work": 1, "Not Available for Work": 0}
  has_one_attached :hero_image 
end
