class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:

        user ||= User.new
        if user.try(:admin?)
          can :dashboard, :all
          can :manage, :all
          can :destroy, User
          can :access, :rails_admin
        end

        /# Free Membership #/
        if user.member_type == 1
          # User can create profile
          
          # User can upload their content
          
          # User can view other content
        end

        /# Basic Membership #/
        if user.member_type == 2
          # ---- User can do everything that a Free user can do.

          # User can update website field

          # User can like and comment on other posts

          # User can request custom commissions and pay `all` commissions

          # User can view all projects but not message or join them
        end

        /# Full Membership #/
        if user.member_type == 3
          # ---- User can do everything that a Free & Basic user can do.

          # User can create a comission page to accept commisions

          # User can create projects and add artists to their projects
        end

        /# Creator Membership #/
        if user.member_type == 0
          # ---- User can do everything, with expedited access to Manufactory on Admin Panel
        end




    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
