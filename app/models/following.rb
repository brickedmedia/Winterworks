    #user_id integer
    #follower_id integer
    #blocked boolean

class Following < ApplicationRecord
    belongs_to :user, :class_name => 'User' # :followed
    belongs_to :follower, :class_name => 'User'


    validates :user_id, uniqueness: {scope: :follower_id}
end