# for the 'marketplace', it's how people on winterworks get paid for their labor
# values: amount, artist, orderer, uuid
# rails g migration AddUUIDToOrder uuid:string
# 
# add status/state
#

class Order < ApplicationRecord
  belongs_to :user
  belongs_to :commission

  validates(:uuid, presence: true, :uniqueness => {:message => "This order has already been made."})
end
