class FollowershipsController < ApplicationController
    def follow_user
        x = current_user
        y = User.find_by_username(params[:username])
        # Check if current user already follows this user
        @z = Following.where(user: y, follower: x).first

        if @z === nil
            puts 'not following, going to follow'
            # User is not already following, continue
            xyz = Following.new(user: y, follower: x)
            xyz.save
            redirect_to view_profile_path(y.username), notice: 'You followed this user'
        else
            # User is already following, unfollow
            puts 'following, going to unfollow'
            y.followers.where(follower_id: x.id).first.destroy
            redirect_to view_profile_path(y.username), notice: 'You unfollowed this user'
        end
    end

    def following
        # list_who_user_is_following
        x = current_user
        y = User.find_by_username(params[:username])
        @following = y.following.all
    end

    def followers
        # list_all_of_their_followers
        y = current_user
        x = User.find_by_username(params[:username])
        @followers = x.followers.all
    end

    def block_user
        #
    end
end

# list all users following X user == Y Followers
# list all users x user follows = Following Y
