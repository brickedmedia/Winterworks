class ManufactoryController < ApplicationController
  include Wicked::Wizard

  steps :select_services, :add_info, :select_style, :order_preview

  def show
    @user = current_user
    case step
    when :order_preview
      #
    end
    render_wizard
  end

  def submit
    render_wizard
  end

#private
  #def safe_previous_wizard_path(id)
  #  puts 'idk'
  #  id ? previous_wizard_path : root_path
  #end
  
  #def finish_wizard_path
  # user_path(current_user)
  #end
end
