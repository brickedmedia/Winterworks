class PostsController < ApplicationController
  skip_before_action :authenticate_user!, :only => [:list_global]

  def upload
    @user = current_user
    @post = @user.posts.new
  end

  def new
    @user = current_user
    @post = @user.posts.create(post_params)

    @post.date_posted = DateTime.now.to_date
    @post.user_id = @user.id
    @post.uuid = SecureRandom.hex(8)
    
    if @post.save
      redirect_to view_profile_path(@user.username), notice: 'A new post was added to your portfolio.'
    else
      redirect_to upload_post_path, notice: 'Missing required attributes for a new post, please try again!'
    end
  end

  def remove
    #
  end

  def list_global
    @users = User.all
    @posts = Post.all
  end

  def view
    @post = Post.find_by_uuid(params[:uuid])
    @poster = User.find(@post.user_id)

    @like = Like.find_by_user_id(current_user.id)

    @comment = @post.comments.new
    @comments = Comment.all
  end

  def like 
    # handles like & dislike
    u = current_user
    @post = Post.find_by(params[:id])
    if @post.likes.where(user: u).count == 0
      @post.like(u)
    else
      @post.unlike(u)
    end

    redirect_to view_this_post_path(@post.uuid)
  end

  private

    def post_params
      params.require(:post).permit(:title, :uuid, :description, :post_type, :post_image, :post_video, :post_audio, :user_id, :date_posted)
    end
end
