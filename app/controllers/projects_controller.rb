# rails g controller projects

class ProjectsController < ApplicationController
    skip_before_action :authenticate_user!, :only => [:feed, :view]

    def feed
        @user = current_user
        @users = User.all
        @projects = Project.all
    end

    def new
        @user = current_user
        @project = @user.projects.new
    end

    def create
        @user = current_user
        @project = @user.projects.create(project_params)

        @project.date_posted = DateTime.now.to_date
        @project.user_id = @user.id
        @project.uuid = SecureRandom.uuid

        if @project.save
            redirect_to view_singular_project_path(@project.uuid), notice: 'This is your new project.'
        else
            redirect_to create_new_commission_path, notice: 'Missing required attributes for a new project, please try again!'
        end
    end

    def view
        @user = current_user
        @project = Project.find_by_uuid(params[:uuid])
        @owner = Project.find_by_uuid(params[:uuid]).user
    end

    def filter_by_project_type
        #render :nothing => true
        # head :ok
        @q = params[:q]
        if @q == 'Volunteer' || 'volunteer'
            @query = 2
            @projects = Project.where(:project_type => @query).all #.all
            render 'shared/_filtered_projects_by_type'
        elsif @q == 'Hobby' || 'hobby'
            @query = 1
            @projects = Project.where(:project_type => @query).all #.all
            render 'shared/_filtered_projects_by_type'
        elsif @q == 'Funded' || 'funded'
            @query = 0
            @projects = Project.where(:project_type => @query).all #.all
            render 'shared/_filtered_projects_by_type'
        else
            puts 'fukck'
        end
    end

    private

    def project_owner_params
        params.require(:project).permit(:user_id)
    end

    def project_params
      params.require(:project).permit(:title, :project_type, :category, :user_id, :is_published, :date_posted, :uuid)
    end
end


