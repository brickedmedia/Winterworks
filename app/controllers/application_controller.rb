class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
 # check_authorization unless: :devise_controller?
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user! # unless: :devise_controller? |||| before every page, authenciate user

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to main_app.root_url, :alert => exception.message
  end

protected

  def after_sign_in_path_for(resource)    
    if current_user.admin?     
      # rails_admin_path 
      admin_root_path    
    else       
      edit_profile_path   
    end       
  end

  def authenticate_admin!
    if current_user && current_user.admin?
      # fine
    else
      redirect_to new_user_session_path
    end
  end

  def configure_permitted_parameters
    # Fields for sign up
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :first_name, :last_name, :paid_member, :member_type])
  end

  # Implement after sign up path to redirect:edit_profile.

end
