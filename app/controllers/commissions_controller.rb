class CommissionsController < ApplicationController
    skip_before_action :authenticate_user!, :only => [:view, :list]

    def create
        @user = current_user
        @commission = @user.commissions.new
    end

    def new
        @user = current_user
        @commission = @user.commissions.create(com_params)
        @commission.user_id = @user.id
        @commission.portfolio_id = @user.portfolio.id
        @commission.uuid = SecureRandom.hex(5)

        if @commission.save
            redirect_to view_user_commission_page_path(@user.username), notice: 'You\'ve added a new commission template to your profile. Users can now request & pay for their own versions of this.'
        else
            puts @commission
            redirect_to create_new_commission_path, notice: 'Missing required attributes for a new commission, please try again!'
        end
    end

    def list
        @users = User.all
        @commissions = Commission.all
    end

    def view
        #
        @user = User.find_by_username(params[:username])
        @commission = @user.commissions.find_by_uuid(params[:uuid])
    end

    # submit order

    private
        def com_params
            params.require(:commission).permit(:title, :desc, :price, :example_image, :user_id, :portfolio_id, :uuid)
        end
end
