class EditProjectController < ApplicationController
    include Wicked::Wizard

    steps :overview, :details, :positions, :preview

    def show
        @user = current_user
        case step
        when :overview
            @project = Project.find_by_uuid(params[:uuid])
        end
        case step
        when :details
            # @user.update_attributes(params[:user])
            @project = Project.find_by_uuid(params[:uuid])
        end
        case step
        when :positions
            @project = Project.find_by_uuid(params[:uuid])
        end
        render_wizard
    end

    def submit
        #
    end
end
