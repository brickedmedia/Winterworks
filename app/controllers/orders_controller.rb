class OrdersController < ApplicationController

    def new
        @user = current_user
        @commission = Commission.find_by_uuid(params[:uuid])
        @owner = @commission.user
    end
end

#generate UUID on create