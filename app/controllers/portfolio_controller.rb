class PortfolioController < ApplicationController
    def show
        @user = User.find_by_username(params[:username])

        if @user == nil 
            redirect_to list_users_path, notice: 'The user you\'ve specified does not exist.'
        end
    end

    def edit 
        @user = current_user
        @portfolio = current_user.portfolio
    end

    def update
        @user = current_user
        @portfolio = current_user.portfolio

        if @portfolio.update_attributes(portfolio_params)
            redirect_to view_user_commission_page_path(@user.username), notice: 'Commission portfolio was updated.'
        else
            redirect_to edit_user_commission_page_path, notice: 'Invalid entry, try again!'
        end
    end

    private
        def portfolio_params
            params.require(:portfolio).permit(:description, :rate_amount, :rate_timeframe, :rate_type)
        end
end
