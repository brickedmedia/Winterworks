class UsersController < ApplicationController   
    skip_before_action :authenticate_user!, :only => [:show, :list]

    def index
        @user = current_user
        @projects = @user.projects
    end

    def notifications
        #
    end

    def show
        x = current_user
        @user = User.find_by_username(params[:username])
        
        @z = Following.where(user: @user, follower: x).first

        if @user == nil 
            redirect_to list_users_path, notice: 'The user you\'ve specified does not exist.'
        end
    end

    def more_info
        @user = User.find_by_username(params[:username])

        if @user == nil 
            redirect_to list_users_path, notice: 'The user you\'ve specified does not exist.'
        end

        @profile = @user.profile
    end

    def list
        @users = User.all.order(created_at: :desc).limit(10)
    end

    def edit_profile
        @user = current_user
    end

    def update_profile
        @user = current_user
        @profile = current_user.profile
        
        if @user.update_attributes(profile_params)
          redirect_to view_profile_path(@user.username), notice: 'User settings were saved.'
        else
            error_msgs = ['THROW ERROR']
            error_msgs << [@user.errors.full_messages]
            redirect_to edit_profile_path, notice: 'Invalid entry, try again!'
        end
    end

    def requests
        @users = User.all.order(created_at: :desc)
    end

    def list_likes
        @user = User.find_by_username(params[:username])
        @likes = @user.likes.count
    end

    private

        def profile_params
            params.require(:user).permit(:first_name, :last_name, :bio, :location, :profile_image, :website, profile_attributes: [:status, :taking_requests, :id])
        end
end
