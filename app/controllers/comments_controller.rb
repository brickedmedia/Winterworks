class CommentsController < ApplicationController
    def create
        @user = current_user
        @post = Post.find_by_uuid(params[:uuid])
        
        @comment = @post.comments.create(comment_params)

        @comment.date_posted = DateTime.now.to_date
        @comment.user_id = @user.id

        if @comment.save
            redirect_to view_this_post_path(@post.uuid)
        else
            redirect_to view_this_post_path(@post.uuid), notice: 'How can you comment without saying anything? Try again.'
        end

    end
        #Comment.new(comment_params

    private

    def comment_params
        params.require(:comment).permit(:body, :user_id, :post_id, :date_posted)
    end
end
