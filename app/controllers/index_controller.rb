class IndexController < ApplicationController
    skip_before_action :authenticate_user!, :only => [:default, :about, :pricing, :discover, :spaces] # skip login needed to view index

    def default
        @users = User.all
    end

    def about
        # TO BE IMPLEMENTED
    end

    def discover
        @users = User.all
        @posts = Post.all
    end

    def pricing
        # TO BE IMPLEMENTED
    end

    def blog
        # TO BE IMPLEMENTED
    end

    def support
        redirect_to 'https://winterworks.zendesk.com/hc/en-us'
    end
    
end
