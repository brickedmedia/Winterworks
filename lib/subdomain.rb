class Subdomain  
    def self.matches?(request)  
      request.subdomain.present?
    end  
end

# request.subdomain.present? && request.host.include?('spaces')